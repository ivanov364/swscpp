//
// Simple Web Server C++ Library
//
// testserver.hpp
//
// Author: Ernest Ivanov
//

#include "testserver.hpp"

sws::testserver::testserver() : serverlib(AF_INET, SOCK_STREAM, 0, 80, INADDR_ANY, 10){
    launch();
}

void sws::testserver::acceptor(){
    struct sockaddr_in address = get_socket()->get_address();
    int addrlen = sizeof(address);
    new_socket = accept(get_socket()->get_sock(), (struct sockaddr *)&address, (socklen_t *)&addrlen);
    read(new_socket, buffer, 30000);
}

void sws::testserver::handler(){
    std::cout << buffer << std::endl;
}

void sws::testserver::responder(){
    char *hello = "Hello from server!";
    write(new_socket, hello, strlen(hello));
    close(new_socket);
}

void sws::testserver::launch(){
    while(true){
        std::cout << "======== WAITING ========\n";
        accepter();
        handler();
        responder();
        std::cout << "======== DONE ========\n";
    }
}