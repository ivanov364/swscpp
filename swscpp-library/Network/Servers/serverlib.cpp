//
// Simple Web Server C++ Library
//
// serverlib.cpp
//
// Author: Ernest Ivanov
//

#include "serverlib.hpp"

sws::serverlib::serverlib(int domain, int service, int protocol, int port, unsigned long interface, int bklg){
    socket = new listeningSock(domain, service, protocol, port, interface, bklg);
}

sws::listeningSock * sws::serverlib::getsocket(){
    return socket;
}