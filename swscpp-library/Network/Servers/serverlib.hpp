//
// Simple Web Server C++ Library
//
// serverlib.hpp
//
// Author: Ernest Ivanov
//

#ifndef serverlib_hpp
#define serverlib_hpp

#include <stdio.h>
#include <iostream>
#include <unistd.h>

#include "../swscpp-network.hpp"

namespace sws{
    class serverlib{
        private:
            listeningSock * socket;
            virtual void acceptor() = 0;
            virtual void handler() = 0;
            virtual void responder() = 0;
        public:
            serverlib(int domain, int service, int protocol, int port, unsigned long interface, int bklg);
            virtual void launch() = 0;
            listeningSock * get_socket();
    };
}

#endif // serverlib_hpp
