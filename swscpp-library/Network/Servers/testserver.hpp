//
// Simple Web Server C++ Library
//
// testserver.hpp
//
// Author: Ernest Ivanov
//

#ifndef testserver_hpp
#define testserver_hpp

#include <stdio.h>
#include <iostream>

#include "serverlib.hpp"

namespace sws{
    class testserver : public serverlib{
        private:
            char buffer[30000] = {0};
            int new_socket;

            void accepter();
            void handler();
            void responder();
        public:
            testserver();
            void launch();
    };
}

#endif // testserver_hpp
