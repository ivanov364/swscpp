//
// Simple Web Server C++ Library
//
// socketlib.cpp
//
// Author: Ernest Ivanov
//

#include "socketlib.hpp"

// Default constructor

sws::socketlib::socketlib(int domain, int service, int protocol, int port, unsigned long interface){
    // Define address structure
    address.sin_family = domain;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = htonl(interface);
    // Establish connection
    sock = socket(domain, service, protocol, port);
    try_connect(sock);
}

// Test connection virtual function

void sws::socketlib::try_connect(int item_to_test){
    // Confirms that the socket or connection has been properly established
    if(item_to_test < 0){
        perror("Failed to connect!");
        exit(EXIT_FAILURE);
    }
}

// Getter functions

struct sockaddr_in sws::socketlib::get_address(){
    return address;
}

int sws::socketlib::get_sock(){
    return sock;
}

int sws::socketlib::get_connection(){
    return connection;
}

// Setter functions

void sws::socketlib::set_connection(int con){
    connection = con;
}