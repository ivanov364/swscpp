//
// Simple Web Server C++ Library
//
// bindingSock.hpp
//
// Author: Ernest Ivanov
//

#ifndef bindingSock_hpp
#define bindingSock_hpp

#include <stdio.h>
#include <iostream>

#include "socketlib.hpp"

namespace sws{
    class bindingSock: public socketlib{
        private:
            int binding;
            // Virtual function from parent
            void connect_to_network(int sock,struct sockaddr_in address);
        public:
            // Constructor
            bindingSock(int domain, int service, int protocol, int port, unsigned long interface);
            
            int get_binding();
    };
}

#endif // bindingSock_hpp
