//
// Simple Web Server C++ Library
//
// swscpp-sockets.hpp
//
// Author: Ernest Ivanov
//

#ifndef swscpp_sockets_hpp
#define swscpp_sockets_hpp

#include <stdio.h>
#include <iostream>

#include "socketlib.hpp"
#include "bindingSock.hpp"
#include "connectSock.hpp"
#include "listeningSock.hpp"

#endif // swscpp_sockets_hpp
