//
// Simple Web Server C++ Library
//
// listeningSock.hpp
//
// Author: Ernest Ivanov
//

#ifndef listeningSock_hpp
#define listeningSock_hpp

#include <stdio.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>

namespace sws{
    class listeningSock : public bindingSock{
        private:
            int backlog;
            int listening;

        public:
            listeningSock(int domain, int service, int protocol, int port, unsigned long interface, int bklg);
            void start_listening();
    };
}

#endif // listeningSock_hpp