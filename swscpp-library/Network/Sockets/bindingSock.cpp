//
// Simple Web Server C++ Library
//
// bindingSock.cpp
//
// Author: Ernest Ivanov
//

#include "bindingSock.hpp"

// Constructor
sws::bindingSock::bindingSock(int domain, int service, int protocol, int port, unsigned long interface) : socketlib(domain, service, protocol, port, interface){
    set_connection(connect_to_network(get_sock(), get_address()));
    try_connect(get_connection());
}

// Definition of connect_to_network virtual function
int sws::bindingSock::connect_to_network(int sock, struct sockaddr_in address){
    return bind(sock, (struct sockaddr *)&address, sizeof(address));
}