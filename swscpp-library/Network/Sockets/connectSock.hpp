//
// Simple Web Server C++ Library
//
// connectSock.hpp
//
// Author: Ernest Ivanov
//

#ifndef connectSock_hpp
#define connectSock_hpp

#include <stdio.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>

namespace sws{
    class connectSock : public socketlib{
        public:
            // Constructor
            connectSock(int domain, int service, int protocol, int port, unsigned long interface);
            // Virtual function from parent
            int connect_to_network(int sock, struct sockaddr_in address);
    };
}

#endif // connectSock_hpp