//
// Simple Web Server C++ Library
//
// connectSock.cpp
//
// Author: Ernest Ivanov
//

#include "connectSock.hpp"

// Constructor
sws::connectSock::connectSock(int domain, int service, int protocol, int port, unsigned long interface) : socketlib(domain, service, protocol, port, interface){
    set_connection(connect_to_network(get_sock(), get_address()));
    try_connect(get_connection());
}

// Definition of the connect_to_network virtual function
int sws::connectSock::connect_to_network(int sock, struct sockaddr_in address){
    return connect(sock, (struct sockaddr *)&address, sizeof(address));
}