//
// Simple Web Server C++ Library
//
// listeningSock.cpp
//
// Author: Ernest Ivanov
//

#include "listeningSock.hpp"

sws::listeningSock::listeningSock(int domain, int service, int protocol, int port, unsigned long interface, int bklg) : bindingSock(domain, service, protocol, port, interface){
    backlog = bklg;
    start_listening();
    test_connection(listening);
}

void sws::listeningSock::start_listening(){
    listening = listen(get_sock(), backlog);
}