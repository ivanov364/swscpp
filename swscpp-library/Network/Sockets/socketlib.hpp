//
// Simple Web Server C++ Library
//
// socketlib.hpp
//
// Author: Ernest Ivanov
//

#ifndef socketlib_hpp
#define socketlib_hpp

#include <stdio.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>

namespace sws{
    class socketlib{
        private:
            struct sockaddr_in address;
            int sock;
           // int connection;

        public:
            // Constructor
            socketlib(int domain, int service, int protocol, int port, unsigned long interface);
            // Virtual function to connect to a network
            virtual int connect_to_network(int sock,struct sockaddr_in address) = 0;
            // Function to test sockets and connections
            void try_connect(int);
            // Getter functions
            struct sockaddr_in get_address();
            int get_sock();
            //int get_connection();
            // Setter functions
            void set_connection(int con);
    };
}

#endif // socketlib_hpp
