//
// Simple Web Server C++ Library
//
// swscpp-network.hpp
//
// Author: Ernest Ivanov
//

#ifndef swscpp_network_hpp
#define swscpp_network_hpp

#include <stdio.h>
#include <iostream>

#include "Sockets/swscpp-sockets.hpp"

#endif // swscpp_network_hpp
