


<img src="img/700175.png">

<h1>SWSCPP Library</h1>

[![Documentation Status](https://readthedocs.org/projects/swscpp/badge/?version=latest)](https://swscpp.readthedocs.io/en/latest/?badge=latest)
<a href='https://codeberg.org/ivanov364/swscpp/src/branch/main/LICENSE'>
    <img src='https://img.shields.io/badge/license-GPL-brightgreen.svg' alt='License' />
</a>

<p><b>SWSCPP</b> - "<b>Simple Web Server C++ Library</b>" for webserver/networking and more.</p>

<h1>Documentation</h1>

<a href="https://swscpp.readthedocs.io/en/latest/">Click to read documentation</a>

<h2>Simple webserver code sample</h2>

<img src="img/sample_img.svg">

<h1>Installation</h1>

```
git clone https://codeberg.org/ivanov364/swscpp.git
cd swscpp/swscpp-library
make
```

<h1>Credits</h1>

<li><p>Thanks to <a href="https://github.com/readthedocs/tutorial-template/">readthedocs.org</a> for docs.</p></li>