Welcome to SWSCPP's documentation!
===================================

**SWSCPP** is a C++ library for webserver and networking
that creates local webserver in few minutes.

Check out the :doc:`usage` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   usage
