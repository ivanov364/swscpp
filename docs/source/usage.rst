Usage
=====

.. _installation:

Installation
------------

To use SWSCPP, first clone it using git:

.. code-block:: console

   git clone https://codeberg.org/ivanov364/swscpp.git
   cd swscpp/swscpp-library
   make

Creating simple webserver
----------------

To create a temporary testserver, you can use ``sws::testserver`` function and name it t;

For example:

.. code-block:: console

   //
   // Simple Web Server C++ Library
   //
   // test.cpp
   //
   // Author: Ernest Ivanov
   //


   #include <iostream>
   #include <stdio.h>

   #include "Network/Servers/testserver.hpp" // Header file for server testing

   int main(){
      sws::testserver t;
   }

